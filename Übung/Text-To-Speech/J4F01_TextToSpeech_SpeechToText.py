#!/usr/bin/env python
# coding: utf-8

# In[1]:


#!pip install gTTS
#!pip install SpeechRecognition
#!pip install C:\Users\43660\anaconda3\pkgs\python-3.8.10-hdbf39b2_7\Scripts\PyAudio-0.2.11-cp38-cp38-win_amd64.whl


# ## Text-To-Speech

# In[2]:


from gtts import gTTS


# In[3]:


tts = gTTS(text='I like NLP and especially Mr. Brunner', lang='en', slow=False)


# In[4]:


tts.save("audio.mp3")


# In[5]:


tts = gTTS(text='I mag NLP und vor alem Herrn Brunner', lang='de', slow=False)


# In[6]:


tts.save("audio_de.mp3")


# ## Speech-To-Text

# In[ ]:


import speech_recognition as sr


# In[ ]:


r = sr.Recognizer()
with sr.Microphone() as source:
    print("Please say something")
    audio = r.listen(source)
    print("Time over, thanks")
    try:
        print("I think you said: "+r.recognize_google(audio))
    except:
        pass

