#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np


# # 01

# In[3]:


df = pd.read_csv("movie.csv")


# In[4]:


df.shape


# In[5]:


df.head()


# # 03

# In[6]:


column = df["movie_title"];
print(type(column))


# # 04

# In[7]:


has_seen=0;
df["has_seen"] = has_seen;
df.head()


# # 05 a)

# In[8]:


director_actor_facebook_likes = df["director_facebook_likes"] + df["actor_3_facebook_likes"] + df["actor_2_facebook_likes"] + df["actor_1_facebook_likes"]
df["director_actor_facebook_likes"] = director_actor_facebook_likes
df.head()


# # b)

# In[ ]:


#Wenn man die NaN werte summiert, wird in jede Spalte ein NaN Wert eingetragen


# # c)

# In[15]:


df.isnull().sum().sum()


# # d)

# In[17]:


df = df.fillna(0)
df


# # 06

# In[19]:


budget = df["budget"].mean()
budget_number = "${:,.2f}".format(budget)
print(budget_number)
#Es ist vergleichweise nicht kostenspielig 


# # 07

# In[24]:


df2 = pd.read_table("sms.txt", header = None)
df2 = df2.rename(columns={0:"label", 1:"message"})
df2


# In[25]:


df2["length"] = df2["message"].str.len()
df2


# In[ ]:




