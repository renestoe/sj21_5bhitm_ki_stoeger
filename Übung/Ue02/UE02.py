#!/usr/bin/env python
# coding: utf-8

# In[6]:


import PyPDF2 as pypdf
import urllib.request as urllib2
from bs4 import BeautifulSoup


# # 2.1

# In[7]:


list = []

with open('text.pdf', 'rb') as pdf:
    reader = pypdf.PdfFileReader(pdf)
    print(reader.getDocumentInfo())


# # 2.2

# In[8]:


list = []

with open('text.pdf', 'rb') as pdf:
    reader = pypdf.PdfFileReader(pdf)
    print(reader.getDocumentInfo())
    page = reader.getPage(0)
    print(page.extractText())
    for page in reader.pages:
        print(page.extractText())
        for page in reader.pages:
            list.append(page)


# In[4]:


for t in list:
    print(t.extractText())


# # 2.3

# In[11]:


response = urllib2.urlopen('https://www.htlkrems.ac.at')
html_doc = response.read()


# In[12]:


soup = BeautifulSoup(html_doc, 'html.parser')


# In[13]:


strhtm = soup.prettify()


# In[14]:


print(strhtm[:100])


# In[15]:


soup.find_all('a')


# In[18]:


for link in soup.find_all('a'):
    print(link.get('href'))


# In[ ]:




