#!/usr/bin/env python
# coding: utf-8

# In[1]:


import spacy
from spacy import displacy 
nlp = spacy.load('en_core_web_sm') 


# # 3.3.1
# 

# In[2]:


doc = nlp('Apple is looking at buying U.K. startup for $1 billion')


# In[3]:


for ent in doc.ents:
    print(ent.text, ent.start_char, ent.end_char, ent.label_,
    spacy.explain(ent.label_))


# In[4]:


displacy.render(doc, style="ent") 


# # 3.3.2

# GPE

# In[5]:


print(spacy.explain("GPE"))


# NORP

# In[6]:


print(spacy.explain("NORP"))


# ORDINAL

# In[7]:


print(spacy.explain("ORDINAL"))


# # 3.3.3

# In[8]:


doc = nlp("Dear Joe! I have organized a meeting with Elon Musk from Siemens for tomorrow. Meeting placeis Vienna." )


# In[9]:


for token in doc.ents:
    if(token.label_ == "PERSON"):
        print(token.text)


# # 3.3.4

# In[10]:


nlp = spacy.load('de_core_news_sm')


# In[11]:


newText = ""


# In[12]:


text ="Der Ministerrat der Republik Österreich beschloss am 25.März 2014, eine „Unabhängige Untersuchungskommission zur transparenten Aufklärung der Vorkommnisse rund um die Hypo Group Alpe-Adria“ einzusetzen. Die Untersuchungskommission (Manuel Ammann, Carl Baudenbacher, Ernst Wilhelm Contzen, Irmgard Griss, Claus-Peter Weber) hat, beginnend mit 1.Mai 2014, durch Auswertung von beigeschafften Unterlagen und allgemein zugänglichen Quellen sowie durch Befragung von Auskunftspersonen den maßgeblichen Sachverhaltfestgestellt und nach fachlichen Kriterien bewertet." 
doc= nlp(text)


# In[13]:


with doc.retokenize() as f:
    for ent in doc.ents:
        f.merge(doc[ent.start:ent.end])


# In[14]:


for token in doc:   
    if(token.ent_type_ =="PER"):
        newText+="[REDACTED] "
    else:
        newText+=token.text+" "
        
print(text)

