#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import re
import nltk
import numpy as np
from nltk.tokenize import word_tokenize 


# ## 3.2

# In[2]:


tweets=[
 'This is introduction to NLP',
 'It is likely to be useful, to people ',
 'Machine learning is the new electrcity',
 'There would be less hype around AI and more action going forward',
 'python is the best tool!','R is good langauage',
 'I like this book','I want more books like this'
] 


# In[3]:


df = pd.DataFrame(tweets, columns=["tweets"])
df["tweets"] = df["tweets"].str.lower()
df


# ## 3.3

# In[4]:


df["tweets"] = df.tweets.str.replace(r"[^a-zA-Z0-9]"," ")
df


# ## 3.4

# In[5]:


#text="Hello there! Welcome to the programming world."
#print(nltk.word_tokenize(text))
#nltk.download('punkt')


# In[6]:


text = "This is the first sentence. This is the second one."
list_of_words = text.split()
print(list_of_words) 


# In[7]:


print(word_tokenize(text)) 


# ##### Der Unterschied zwischen den beiden Ausgaben ist, dass beim "word_tokenize"
# #####  jeden Token einzeln erkennt, z.B. unterscheidet es ein Satzzeichen von 
# #####  einem string. 
# #####  Bei .split() Wird das nicht unterschieden, weil man es selbst noch dazu 
# #####  definieren müsste

# ## 3.5

# In[8]:


#token = df["tweets"].str.split()
#token
df["tokenized"] = df["tweets"].apply(word_tokenize)
#word_tokenize(str(df["tweets"]))
df


# ## 3.6

# In[9]:


data = open('dataScientistJobDesc.txt', 'r')
data_text=data.read()
data_text


# In[10]:


dsj = word_tokenize(data_text)
len(dsj)


# In[11]:


#dsj = np.unique(dsj)
#len(dsj)
dsj2 = set(dsj)
len(dsj2)


# In[ ]:




