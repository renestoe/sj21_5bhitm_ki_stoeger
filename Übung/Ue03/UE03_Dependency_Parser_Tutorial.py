#!/usr/bin/env python
# coding: utf-8

# # Dependency Parser and Dependency Tree Visualizer in Spacy

# In[1]:


import spacy


# In[2]:


nlp=spacy.load('en_core_web_sm')


# In[3]:


text='I was heading towrds North'

print(f"{'Token':{8}} {'dependence':{12}} {'head text':{9}}  {'Dependency explained'} ")
for token in nlp(text):
     print(f"{token.text:{8}} {token.dep_+' =>':{10}}   {token.head.text:{9}}  {spacy.explain(token.dep_)} ")


# In[4]:


parser_lst = nlp.pipe_labels['parser']

print(len(parser_lst))
print(parser_lst)


# In[5]:


import spacy
from spacy import displacy


# In[6]:


doc = nlp(text)
displacy.render(doc,jupyter=True)


# In[7]:


for token in doc:
    print(token.text, token.dep_, token.head.text, [child for child in token.children])


# In[ ]:




