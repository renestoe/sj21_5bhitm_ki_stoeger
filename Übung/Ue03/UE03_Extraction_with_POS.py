#!/usr/bin/env python
# coding: utf-8

# In[1]:


import spacy
from spacy.matcher import Matcher


# In[2]:


nlp = spacy.load("en_core_web_sm")
matcher = Matcher(nlp.vocab)


# In[3]:


doc = nlp("After making the iOS update you won't notice a radical system-wide " 

    "redesign: nothing like the aesthetic upheaval we got with iOS 7. Most of " 
    "iOS 11's furniture remains the same as in iOS 10. But you will discover " 
    "some tweaks once you delve a little deeper.")


# In[4]:


pattern = [{"TEXT" : "iOS"}, {"IS_DIGIT": True}]


# In[5]:


matcher.add("IOS_VERSION_PATTERN", [pattern])
matches = matcher(doc)
print("Total matches found:", len(matches))


# In[6]:


for match_id, start, end in matches:
    print("Match found:", doc[start:end].text)


# In[7]:


doc = nlp("i downloaded Fortnite on my laptop and can't open the game at all. Help? so when I was downloading Minecraft, I got the Windows version where it is the '.zip' folder and I used the default program to unpack it... do I also need to download Winzip?")


# In[8]:


pattern = [{"LEMMA": "download"}, {"POS": "PROPN"}]


# In[9]:


doc = nlp("Features of the app include a beautiful design, smart search, automatic labels and optional voice responses.")


# In[10]:


pattern = [{"POS": "ADJ"}, {"POS": "NOUN"}, {"POS": "NOUN", "OP": "?"}]


# In[11]:


matcher.add("Words", [pattern])
matches = matcher(doc)
print("Total matches found:", len(matches))


# In[12]:


for match_id, start, end in matches:
    print("Match found:", doc[start:end].text)


# In[13]:


matcher = Matcher(nlp.vocab)


# ## 3.3.1

# In[14]:


import spacy 
from spacy.matcher import Matcher 


# In[15]:


nlp = spacy.load("en_core_web_sm")
matcher = Matcher(nlp.vocab)


# In[16]:


pattern = [{"LOWER": "hello"}, {"IS_PUNCT": True, "OP": "?"}, {"LOWER": "world"}]
matcher.add("HelloWorld", [pattern])


# In[17]:


text = nlp("Hello, world! Hello world!")
matches = matcher(text)
for match_id, start, end in matches:
    string_id = nlp.vocab.strings[match_id]
    span = text[start:end]
    print(match_id, string_id, start, end, span.text)


# ## 3.3.2

# In[18]:


with open('dataScientistJobDesc.txt', encoding='utf-8') as f:
    data_text = f.read()

matcher = Matcher(nlp.vocab)

doc = nlp(data_text)

patterns = [
    [{"POS": "NOUN"}, {"LOWER": "experience"}],
    [{"LOWER": "experience"}, {"LEMMA": {"IN": ["with", "in", "as"]}}, {"POS": {"in": ["NOUN", "PROPN"]}, "OP": "+"}],
    [{"LOWER": "experience"}, {"LEMMA": {"IN": ["with", "in", "as"]}}, {"POS": {"in": ["NOUN", "PROPN"]}, "OP": "+"}, {"LEMMA": "or", "OP": "?"}, {"POS": {"in": ["NOUN", "PROPN"]}, "OP": "+"}]
]

matcher.add("EXPERIENCE_PATTERNS", patterns)
matches = matcher(doc)

for match_id, start, end in matches:
    print(doc[start:end])

