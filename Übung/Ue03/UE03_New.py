#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import re
import nltk
import numpy as np
from nltk.tokenize import word_tokenize
from nltk.tokenize import sent_tokenize
from nltk.tokenize import WordPunctTokenizer
import spacy
from nltk.stem import PorterStemmer
from nltk.stem import WordNetLemmatizer


# ## 3.2

# In[2]:


tweets=[
 'This is introduction to NLP',
 'It is likely to be useful, to people ',
 'Machine learning is the new electrcity',
 'There would be less hype around AI and more action going forward',
 'python is the best tool!','R is good langauage',
 'I like this book','I want more books like this'
] 


# In[3]:


df = pd.DataFrame(tweets, columns=["tweets"])
df["tweets"] = df["tweets"].str.lower()
df


# ## 3.3

# In[4]:


df["tweets"] = df.tweets.str.replace("[^a-zA-Z0-9]"," ")
df


# ## 3.4

# In[5]:


#text="Hello there! Welcome to the programming world."
#print(nltk.word_tokenize(text))
#nltk.download('punkt')


# In[6]:


text = "This is the first sentence. This is the second one."
list_of_words = text.split()
print(list_of_words) 


# In[7]:


print(word_tokenize(text)) 


# ##### Der Unterschied zwischen den beiden Ausgaben ist, dass beim "word_tokenize"
# #####  jeder Token einzeln erkennt wird, z.B. unterscheidet es ein Satzzeichen von 
# #####  einem string. 
# #####  Bei .split() Wird das nicht unterschieden, weil man es selbst noch dazu 
# #####  definieren müsste

# ## 3.5

# In[8]:


#token = df["tweets"].str.split()
#token
df["tokenized"] = df["tweets"].apply(word_tokenize)
#word_tokenize(str(df["tweets"]))
df


# ## 3.6

# In[9]:


data = open('dataScientistJobDesc.txt', 'r')
data_text=data.read()
data_text


# In[10]:


dsj = word_tokenize(data_text)
len(dsj)


# In[11]:


#dsj = np.unique(dsj)
#len(dsj)
dsj2 = set(dsj)
len(dsj2)
type(dsj2)


# In[12]:


import spacy


# In[13]:


nlp = spacy.load("de_core_news_sm") 
doc = nlp("Apple erwägt den Kauf eines österreichischen Startups um 6 Mio. Euro.") 
print(doc)


# In[14]:


for token in doc: 
    print(f"{token.text:{20}}",type(token))


# ## 3.7

# In[15]:


nlp = spacy.load("de_core_news_sm")
doc = nlp("Apple erwägt den Kauf eines österreichischen Startups um 6 Mio. Euro.")
print(doc) 


# In[16]:


for token in doc:
    print(f"{token.text:{20}}",type(token)) 


# In[17]:


data = open('dataScientistJobDesc.txt', 'r', encoding = 'utf-8')
data_text = data.read()


# In[18]:


nlp = spacy.load("de_core_news_sm")
doc = nlp(data_text)
len(doc)


# In[19]:


set_doc = []
seen = set()
for word in doc:
    if word.text not in seen:
        set_doc.append(word)
    seen.add(word.text)
len(set_doc)


# ## 3.8

# In[20]:


#nltk.download('stopwords')
stop = nltk.corpus.stopwords.words('english')
text = "How to develop a chatbot in Python"
result = [word for word in text.split() if word not in stop]
result


# ## 3.9

# In[21]:


pt = PorterStemmer()
words = ['run','runner','running','ran','runs']
[pt.stem(word) for word in words]


# In[22]:


df['tweets'] = df['tweets'].apply(lambda x : " ".join([word for word in x.split() if word not in stop]))
df


# ## 3.10

# In[24]:


#nltk.download('wordnet')
text=['I like fishing','I eat fish','There are many fishes in pound'] 
[pt.stem(words) for words in text]


# In[25]:


lemmatizer = WordNetLemmatizer()
print("rocks:", lemmatizer.lemmatize("rocks"))
print("studies:", lemmatizer.lemmatize("studies"))
print("better :", lemmatizer.lemmatize("better", pos ="a")) 


# ## 3.11

# In[26]:


text=['I like fishing','I eat fish','There are many fishes in pound'] 
text_s = pd.Series(text)
text_s.apply(lambda x: " ".join([lemmatizer.lemmatize(word) for word in x.split()]))

