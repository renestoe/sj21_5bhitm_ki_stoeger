#!/usr/bin/env python
# coding: utf-8

# In[1]:


#!/usr/bin/env python
# coding: utf-8


# In[2]:


from nltk.util import ngrams
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import CountVectorizer 
import pandas as pd
from collections import Counter
import nltk


# In[3]:


text = "I love NLP and I will learn NLP in two month" 
n=2


# In[4]:


bigrams= ngrams(text.split(),n)
for bigram in bigrams:
    print(bigram)


# In[5]:


#CountVectorizer
vectorizer = CountVectorizer(ngram_range=(2,2))
vectorizer.fit([text]) 
vectorizer.vocabulary_


# In[6]:


#4.2.1
#love nlp
#two month
#nlp in


# # 4.2.2

# In[7]:


def ngram_trump(n):
    df_trump = pd.read_csv("realdonaldtrump.csv")
    content=df_trump["content"].array
    stop_words = set(stopwords.words('english'))
    tokenized_words= [nltk.word_tokenize(sent)for sent in content]
    #Remove punct
    tokenized_words_al= [[x for x in ls if x.isalnum()] for ls in tokenized_words ]
    #Remove stop_words 
    trump_tokens = [[x.lower() for x in ls if not x.lower() in stop_words]for ls in tokenized_words_al]
    bigrams_trump=[ngrams(t,n) for t in trump_tokens]
    #unwind_bigrams array
    bigrams_trump_new = [item for sublist in bigrams_trump for item in sublist]
    c = Counter(bigrams_trump_new)
    return c.most_common(3)


# In[8]:


ngram_trump(2)


# In[9]:


ngram_trump(3)


# In[ ]:





# In[ ]:




