#!/usr/bin/env python
# coding: utf-8

# In[1]:


from tensorflow.keras.utils import to_categorical
from sklearn.feature_extraction.text import CountVectorizer


# In[2]:


import spacy
nlp=spacy.load('en_core_web_sm')


# In[3]:


text="Jim loves NLP. He will learn NLP in two months. NLP is future."

def get_spacy_tokens(text):
    
    doc = nlp(text)
    
    return [token.text for token in doc]
text_tokens = get_spacy_tokens(text)

vectorizer = CountVectorizer(tokenizer=get_spacy_tokens, lowercase=False)

# Erstellung des Vokabulars:
vectorizer.fit(text_tokens)
print("Vocabulary: ", vectorizer.vocabulary_)
vocab= vectorizer.vocabulary_
print(vocab['Jim'])


# In[4]:


LENGTH_OF_VOCAB=12
to_categorical(vocab['learn'],LENGTH_OF_VOCAB)


# In[5]:


# Erstellung der Matrix mit Wortvektoren (One Hote Encoding):
vector = vectorizer.transform(text_tokens)
print("Encoded Document is:")
print(vector.toarray())


# # 4.1

# In[6]:


def one_hot_encoding(sent):
    doc = nlp(sent)
    vect_num = []
    for token in doc:
        for key, value in vocab.items():
            if token.text == key:
                vect_num.append(value)
    return to_categorical(vect_num)


# In[7]:


print(one_hot_encoding("Jim will learn NLP in future."))

