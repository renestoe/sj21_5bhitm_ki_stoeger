#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer,TfidfVectorizer
from sklearn import model_selection, preprocessing, linear_model, naive_bayes, metrics, svm


# In[3]:


from nltk.corpus import stopwords
stop = stopwords.words('english')
data = pd.read_csv ('spam.csv', encoding='iso-8859-1')
data = data.drop(columns=['Unnamed: 2', 'Unnamed: 3', 'Unnamed: 4'])
data = data.rename(columns={"v1": "target", "v2": "message"})
data["message"] = data["message"].str.lower()
data['message'] = data['message'].apply(lambda x: ' '.join([word for word in x.split() if word not in (stop)]))
data['message'] = data['message'].str.replace("!","")
data['message'] = data['message'].str.replace("?","")
data['message'] = data['message'].str.replace(",","")
data['message'] = data['message'].str.replace(".","")
from nltk.stem import WordNetLemmatizer
lemmatizer = WordNetLemmatizer()
data['message'] = data['message'].apply(lambda x: ' '.join([lemmatizer.lemmatize(word) for word in x.split()]))
data


# In[4]:


train_x, valid_x, train_y, valid_y = train_test_split(data["message"],data["target"], test_size=.3)
print("Check shape: ", train_x.shape, valid_x.shape)


# In[5]:


encoder = preprocessing.LabelEncoder()
train_y = encoder.fit_transform(train_y)
valid_y = encoder.fit_transform(valid_y)

print("Encoder hat mit 0 und 1 zwei Klassen erkannt: ", encoder.classes_)
print("Die kodierten Antworten der Trainingsdaten: " , train_y)


# In[6]:


vectorizer = TfidfVectorizer(analyzer='word', max_features=5000)
vectorizer.fit(data['message'])


# In[7]:


train_x_tfidf = vectorizer.transform(train_x)
valid_x_tfidf = vectorizer.transform(valid_x)
print("Check expected shape for train data: ", train_x_tfidf.shape)
print("Check expected shape for validation data: ", valid_x_tfidf.shape)


# In[8]:


clf = naive_bayes.MultinomialNB(alpha=0.2)
clf.fit(train_x_tfidf, train_y)
predictions = clf.predict(valid_x_tfidf)
metrics.accuracy_score(predictions, valid_y)

