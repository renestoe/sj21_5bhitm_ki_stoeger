#!/usr/bin/env python
# coding: utf-8

# In[1]:


from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import spacy
import pandas as pd


# In[2]:


documents = (
"I like NLP",
"I am exploring NLP",
"I am a beginner in NLP",
"I want to learn NLP",
"I like advanced NLP")


# In[3]:


tfidf_vectorizer=TfidfVectorizer()
tfidf_matrix = tfidf_vectorizer.fit_transform(documents)
tfidf_matrix.toarray()


# In[4]:


cosine_similarity(tfidf_matrix[0:1],tfidf_matrix)


# In[5]:


get_ipython().system('python -m spacy download en_core_web_md')


# In[6]:


nlp=spacy.load("en_core_web_md")
cats_token, dogs_token = nlp("cats dogs")
cats_token.similarity(dogs_token)


# In[7]:


cats_token.vector


# In[8]:


sentence = "This is a sentence."
nlp(sentence).vector_norm


# In[9]:


doc1 = nlp("I like NLP")
doc2 = nlp("I like advanced NLP")
doc1.similarity(doc2)


# In[10]:


def calculate_similarity(request):
    data = pd.read_csv("covid_faq.csv")
    request = nlp(request)
    questions = [nlp(question) for question in data["questions"]]
    similiarities = [request.similarity(question) for question in questions]
    result = max(similiarities)
    index = similiarities.index(result)
    row = data.iloc[index]
    return row['answers']


# In[11]:


calculate_similarity("school")


# In[12]:


df = pd.read_csv('room_type.csv')
df.head(10)


# In[13]:


get_ipython().system('pip install fuzzywuzzy')


# In[14]:


from fuzzywuzzy import fuzz
fuzz.ratio('Deluxe Room, 1 King Bed','Deluxe King Room')


# In[15]:


fuzz.ratio('Traditional Double Room, 2 Double Beds','Double Room with Two Double Beds')


# In[16]:


fuzz.ratio('Room, 2 Double Beds (19th to 25th Floors)','Two Double Beds - Location Room (19th to 25th Floors)')


# In[17]:


fuzz.partial_ratio('Deluxe Room, 1 King Bed','Deluxe King Room')


# In[18]:


fuzz.partial_ratio('Traditional Double Room, 2 Double Beds','Double Room with Two Double Beds')


# In[19]:


fuzz.partial_ratio('Room, 2 Double Beds (19th to 25th Floors)','Two Double Beds - Location Room (19th to 25th Floors)')


# In[20]:


fuzz.token_sort_ratio('Deluxe Room, 1 King Bed','Deluxe King Room')


# In[21]:


fuzz.token_sort_ratio('Traditional Double Room, 2 Double Beds','Double Room with Two Double Beds')


# In[22]:


fuzz.token_sort_ratio('Room, 2 Double Beds (19th to 25th Floors)','Two Double Beds - Location Room (19th to 25th Floors)')


# In[23]:


fuzz.token_set_ratio('Deluxe Room, 1 King Bed','Deluxe King Room')


# In[24]:


fuzz.token_set_ratio('Traditional Double Room, 2 Double Beds','Double Room with Two Double Beds')


# In[25]:


fuzz.token_set_ratio('Room, 2 Double Beds (19th to 25th Floors)','Two Double Beds - Location Room (19th to 25th Floors)')


# In[26]:


def get_ratio(row):
    name = row['Expedia']
    name1 = row['Booking.com']
    return fuzz.token_set_ratio(name,name1)
len(df[df.apply(get_ratio, axis=1)>70])/len(df)


# In[27]:


fuzz.ratio('mahul','mehul')


# In[28]:


fuzz.ratio('Mehul Gupta','Mehul')


# In[29]:


fuzz.ratio('mehullllllllll','mehul')


# In[30]:


get_ipython().system('pip install jellyfish')


# In[31]:


import jellyfish
code1 = jellyfish.soundex('mehul')
print(code1)
code2 = jellyfish.soundex('mahul')
print(code2)
print('fuzz ratio for oraiginal strings')
print(fuzz.ratio('mehul', 'mahul'))
print('fuzz ratio for code1 & code2')
print(fuzz.ratio(code1, code2))


# In[32]:


print(jellyfish.soundex('mehulllllllll'))
print(jellyfish.soundex('mehul'))


# In[33]:


code1 = jellyfish.soundex('gaurav gupta')
print(code1)
code2 = jellyfish.soundex('gaurav')
print(code2)
print('fuzz ratio for oraiginal strings')
print(fuzz.ratio('gaurav gupta', 'gaurav'))
print('fuzz ratio for code1 & code2')
print(fuzz.ratio(code1, code2))


# In[34]:


print(jellyfish.soundex('mehullllllllll'))
print(jellyfish.soundex('mehul'))


# In[35]:


print(jellyfish.soundex('muralitharan'))
print(jellyfish.soundex('muralith'))
print(jellyfish.soundex('muralithzzzzzzzzzz'))


# In[36]:


print(jellyfish.soundex('gaurav deshmukh'))
print(jellyfish.soundex('gaurav dwivedi'))
print(jellyfish.soundex('gaurav dravid'))

