#!/usr/bin/env python
# coding: utf-8

# In[13]:


import pandas as pd


# # Step 0

# In[15]:


df = pd.read_csv("consumer_complaints.csv")


# In[ ]:


df.head()


# # Step 1

# In[ ]:


df = df[["product","consumer_complaint_narrative"]].dropna()
df


# # Step 2

# In[ ]:


print(df['product'].value_counts())
print("Kategorien: " + str(len(df['product'].value_counts())))


# # Step 3

# In[ ]:


from sklearn.model_selection import train_test_split
x_train, x_test, y_train, y_test = train_test_split(df["consumer_complaint_narrative"],df["product"], test_size=.3, random_state=1234)

print("Check shape: ", x_train.shape, x_test.shape, y_train.shape, y_test.shape)


# # Step 4

# In[ ]:


from sklearn import preprocessing


# In[ ]:


le = preprocessing.LabelEncoder()
le.fit(y_train)
print(len(le.classes_))
y_train = le.transform(y_train)
y_test = le.transform(y_test)

print(y_train)


# In[ ]:


from sklearn.feature_extraction.text import TfidfVectorizer
vectorizer = TfidfVectorizer(analyzer='word', max_features=3000)
vectorizer.fit(df['consumer_complaint_narrative'])


# In[ ]:


x_train_tfid = vectorizer.transform(x_train)
x_test_tfid = vectorizer.transform(x_test)


# In[ ]:


print(x_train_tfid.shape)
print(x_test_tfid.shape)


# # Step 5

# In[ ]:


from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics


# In[ ]:


clf = RandomForestClassifier(n_estimators=100)


# In[ ]:


clf.fit(x_train_tfid, y_train)


# In[ ]:


y_pred = clf.predict(x_test_tfid)


# In[ ]:


print("Accuracy:", metrics.accuracy_score(y_test,y_pred))


# In[ ]:


from sklearn.metrics import classification_report


# In[ ]:


report=classification_report(y_test, y_pred)
print(report)

