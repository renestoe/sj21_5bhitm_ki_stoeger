#!/usr/bin/env python
# coding: utf-8

# In[2]:


import os
import codecs
import spacy
from sklearn.feature_extraction.text import CountVectorizer


# # Next Word Prediction I
# ##### Step I - Preparing Data
# 
# 
# 
# 

# ### 9.2 - sämtliche TXT-Files in ein Notebook laden

# Laden Sie sämtliche TXT-FIles und fügen Sie diese in einem Notebook zusammen. Der Inhalt ist der Variable text zuzuweisen.

# In[3]:


paths = ["text/Meinungsrede_Handyverbot/", "text/Textinterpretation_LieferungFreiHaus/", "text/Zusammenfassung_Klima/"]

text = ""
for path in paths:
    text += ''.join([codecs.open(f, 'r', 'utf-8').read() for f in [path+f for f in os.listdir(path)]])
print(text)


# ### 9.3 Tokenisierung

# Führen Sie die Tokenisierung des Textes in text durch. Umsetzung mit Spacy, und
# zwar mit dem deutschen Sprachmodell. Speichern Sie das Ergebnis (als Text (=Typ String))
# in tokens_all. Ermitteln Sie die Anzahl der Tokens in tokens_all. Entferne Sie Duplikate
# und ermitteln Sie wiederum die Anzahl. Vergleichen Sie diese mit ihrem/ihrer Kollegen/
# Kollegin.

# In[5]:


nlp = spacy.load('de_core_news_sm')
doc = nlp(text)


# In[6]:


print(f"Length of Text: {len(text)}")
tokens_all = [token.text for token in doc]
print(f"Length of tokens_all: {len(doc)}")
print(f"Type of tokens_all: {type(tokens_all[1])}")
tokens_all = set(tokens_all)
print(f"Without Dupicates: {len(tokens_all)}")


# ### 9.4 - Reduktion mit CountVectorizer

# a) Nehmen Sie im ersten Schritt die Inhalte der ursprünglichen Variable tokens_all (siehe
# Aufgabe 9.3., gelbe Markierung) und Verwenden Sie den CountVectorizer in der Default-
# Konfiguration.

# In[8]:


cv = CountVectorizer()
cv.fit(tokens_all)
features = cv.get_feature_names()
features


# Es sollte ersichtlich sein, dass der CountVectorizer Satzzeichen entfernt und alles lower case
# ist. Das liegt an den zuvor erwähnten Gründen.

# b) Mit Verwendung von lowercase=False und token_pattern=.* gelangt man zum
# gewünschten Ergebnis.

# In[10]:


cv = CountVectorizer(lowercase=False, token_pattern='.*')
cv.fit(tokens_all)
features = cv.get_feature_names()
features


# c) Abschließend ist jetzt noch die Anzahl der Tokens auf die eingangs geforderten 1000
# oder 2000 zu reduzieren. Hierzu: max_features=2000 verwenden.…

# In[11]:


cv = CountVectorizer(max_features=2000, lowercase=False, token_pattern='.*')
cv.fit(tokens_all)
features = cv.get_feature_names()
features
len(features)


# ### 9.5 - Dictonaries word_to_int and word_to_string

# Implementieren Sie jenen Algorithmus, den es braucht, um die zwei Dictionaries
# word_to_int und int_to_word zu erstellen. Ausgangsbasis bildet das auf 1000 oder
# 2000 reduzierte Vokabular features.
# - word_to_int: Anhand eines Wortes (o. Satzzeichens) erhalte ich die ID (v. Typ int)
# - int_to_word: Anhand der ID erhalte ich das Wort (o. Satzzeichens).

# In[12]:


word_to_int = {}
[word_to_int.update({w: features.index(w)}) for w in features]
print(type(word_to_int))
print(word_to_int)

int_to_word = {}
[int_to_word.update({features.index(w): w}) for w in features]
print(type(int_to_word))
print(int_to_word)


# ### 9.6 - Füllen der Liste token_transformed

# Erstellen bzw. befüllen Sie die Liste token_transformed gemäß Anforderung. Ermitteln
# Sie die Anzahl der Einträge und vergleichen Sie diese mit token_all.

# In[13]:


token_transformed = [word_to_int[token] for token in tokens_all if token in word_to_int.keys()]
print(f"Length of tokens_all: {len(tokens_all)}")
print(f"Length of token_transformed: {len(token_transformed)}")

