#!/usr/bin/env python
# coding: utf-8

# # Übung 09: Next Word Prediction 1

# In[2]:


import pandas as pd
import spacy
from sklearn.feature_extraction.text import CountVectorizer
import os


# ## 9.2 Text einlesen

# In[3]:


text = ""

your_path = 'text'
files = os.listdir(your_path)
for file in files:
    if os.path.isfile(your_path + "/" + file):
        with open(your_path + "/" + file, 'r', encoding='UTF-8') as f:
            text = text + f.read()
            
print(text)


# ## 9.3 Tokenisierung

# In[4]:


tokens_all = []
nlp= spacy.load("de_core_news_sm")
doc = nlp(text)
tokens_all = [token.text for token in doc]

print("Mit Duplikaten: " + str(len(tokens_all)))


unique_tokens = [tokens_all.text for tokens_all in doc]
unique_tokens = set(unique_tokens)
print("Ohne Duplikate: " + str(len(unique_tokens)))


# ## 9.4 CountVectorizer

# In[5]:


#b)
cv = CountVectorizer(lowercase=False, token_pattern='.*')
cv.fit(tokens_all)
features = cv.get_feature_names()
#print(features)
print(str(len(features)))

#c)
cv2 = CountVectorizer(lowercase=False, token_pattern='.*', max_features=300)
cv2.fit(tokens_all)
features2 = cv2.get_feature_names()
#print(features2)
print( str(len(features2)))


# ## 9.5 Dictonaries word_to_int and word_to_string

# In[6]:


word_to_int = {}
[word_to_int.update({w: features2.index(w)}) for w in features2]
print(type(word_to_int))
print(word_to_int)
print('--------------------------------------------------------------------------------------------------------------------\n')
int_to_word = {}
[int_to_word.update({features2.index(w): w}) for w in features2]
print(type(int_to_word))
print(int_to_word)


#word_to_int = {}
#int_to_word = {}
#for i in range(0, len(features))
#    word = features[i]
#    word_to_int[word] = i
#    int_to_word[i] = word
#print(int_to_word)


# ## 9.6 Transform

# In[7]:


#token_transformed = [word_to_int[token] for token in tokens_all if token in word_to_int.keys()]
#print(f"Length of tokens_all: {len(tokens_all)}")
#print(f"Length of token_transformed: {len(token_transformed)}")

token_transformed = [word_to_int[token] for token in tokens_all if token in word_to_int]
print(len(token_transformed))
print(token_transformed[:100])


# ## 9.7 Textsequenzen

# In[13]:


X = []
Y = []
sequence_length = 40

for i in range(len(tokens_all)-sequence_length-1):
    X.append(tokens_all[i:i+sequence_length])
    Y.append(tokens_all[i+sequence_length+1])
    

#print(X[:2].shape)
print(X[:2])
print('--------------------------------------------------------------')
#print(Y[:2].shape)
print(Y[:2])


# In[ ]:




